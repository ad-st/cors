package main

import (
	"net/http"
)

type handler struct {
	allowedOrigin string
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if origin := r.Header.Get("Origin"); origin == h.allowedOrigin {

		switch r.Method {
		case "GET":
			fallthrough
		case "PUT":
			w.Header().Set("Access-Control-Allow-Origin", origin)
			http.FileServer(http.Dir('.')).ServeHTTP(w, r)
		case "OPTIONS":
			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
			w.Header().Set("Access-Control-Allow-Methods", "GET, PUT")
		}

	}
}

func main() {

	go func() {
		if err := http.ListenAndServe(":8080", http.FileServer(http.Dir('.'))); err != nil {
			panic(err)
		}
	}()

	http.ListenAndServe(":8090", handler{"http://localhost:8080"})
}
